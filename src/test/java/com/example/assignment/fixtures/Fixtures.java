package com.example.assignment.fixtures;

import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserClientForm;

public class Fixtures {

    public static UserClientForm userClientForm() {
        UserClientForm userClientForm = new UserClientForm();
        userClientForm.setFirstName("John");
        userClientForm.setLastName("Doe");
        userClientForm.setUsername("random_username");
        userClientForm.setAddress("Tallinn");
        userClientForm.setEmail("test@test.com");
        userClientForm.setCountry("EE");
        return userClientForm;
    }

    public static UserClient userClient() {
        return UserClient.builder()
                .withUserName("random")
                .withFirstName("Mike")
                .withLastName("Tyson")
                .withEmail("test@test.com")
                .withAddress("Los Angeles")
                .withCountry("EE")
                .build();
    }
}

package com.example.assignment.repositories;

import com.example.assignment.fixtures.Fixtures;
import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static com.example.assignment.model.UserName.userName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class UserClientsRepositoryTest extends DatabaseTest {

    private UserClientsRepository userClientsRepository;

    private UserName userName = userName("test_user");

    @BeforeEach
    void setUp() {
        cleanup();
        userClientsRepository = new UserClientsRepository(namedJdbcTemplate);
    }

    private UserClient saveClientFor(UserName userName) {
        return saveClientFor(userName, "randomClient");
    }

    private UserClient saveClientFor(UserName userName, String clientUsername) {
        UserClient userClient = UserClient.builder()
                .withUserName(clientUsername)
                .withFirstName("FirstName")
                .withLastName("LastName")
                .withEmail("test@test.com")
                .withAddress("Tallinn")
                .withCountry("Estonia")
                .build();
        userClientsRepository.saveUserClient(userName, userClient);
        return userClient;
    }

    @Nested
    public class FindUserClients {
        @Test
        void userHasNoClients() {
            List<UserClient> userClients = userClientsRepository.findUserClients(userName);
            assertThat(userClients, hasSize(0));
        }

        @Test
        void userHasOneClient() {
            UserClient client = saveClientFor(userName);

            List<UserClient> userClients = userClientsRepository.findUserClients(userName);
            assertThat(userClients, hasSize(1));

            UserClient userClient = userClients.get(0);
            assertThat(userClient, equalTo(client));
        }

        @Test
        void userHasMultipleClients() {
            UserClient client1 = saveClientFor(userName, "client1");
            UserClient client2 = saveClientFor(userName, "client2");
            UserClient client3 = saveClientFor(userName, "client3");

            List<UserClient> userClients = userClientsRepository.findUserClients(userName);

            assertThat(userClients, hasSize(3));
        }

        @Test
        void findsOnlyCurrentUserClients() {
            UserClient client1 = saveClientFor(userName, "client1");
            saveClientFor(userName("other_user"), "client2");

            List<UserClient> userClients = userClientsRepository.findUserClients(userName);

            assertThat(userClients, hasSize(1));
            assertThat(userClients.get(0).getUserName(), equalTo(client1.getUserName()));
        }
    }

    @Nested
    public class SaveUserClient {
        private UserName userName = userName("random_user");

        @Test
        void saveUserClient() {
            UserClient client = Fixtures.userClient();

            userClientsRepository.saveUserClient(userName, client);

            List<UserClient> userClients = userClientsRepository.findUserClients(userName);

            assertThat(userClients, hasSize(1));
        }

        @Test
        void updateExistingClient() {

            UserClient initialUserClient = saveClientFor(userName);

            Optional<UserClient> userClientByUsername =
                    userClientsRepository.findUserClientByUsername(userName, initialUserClient.getUserName());

            assertThat(userClientByUsername.isPresent(), equalTo(true));

            UserClient newUserClient = UserClient.builderFrom(initialUserClient)
                    .withLastName("NEW LAST NAME")
                    .build();

            userClientsRepository.saveUserClient(userName, newUserClient);
            Optional<UserClient> newUserClientOptional =
                    userClientsRepository.findUserClientByUsername(userName, initialUserClient.getUserName());

            assertThat(newUserClientOptional.isPresent(), equalTo(true));
            assertThat(newUserClientOptional.get().getLastName(), equalTo(newUserClient.getLastName()));
        }
    }

    @Nested
    public class FindSingleUserClient {
        private UserName userName = userName("random_user");

        @Test
        void emptyOptionalWhenUserDoesntExist() {
            Optional<UserClient> userClient =
                    userClientsRepository.findUserClientByUsername(userName, "non_exitsing_user");

            assertThat(userClient.isPresent(), equalTo(false));
        }

        @Test
        void returnsOptionalWithUserClient() {
            UserClient actualUserClient = saveClientFor(userName);

            Optional<UserClient> userClient =
                    userClientsRepository.findUserClientByUsername(userName, actualUserClient.getUserName());

            assertThat(userClient.isPresent(), equalTo(true));
        }

        @Test
        void cantFindOtherUsersClient() {
            saveClientFor(UserName.userName("other_user"), "client1");
            UserClient actualClient = saveClientFor(userName);

            Optional<UserClient> userClient =
                    userClientsRepository.findUserClientByUsername(userName, actualClient.getUserName());

            assertThat(userClient.isPresent(), equalTo(true));
            assertThat(userClient.get().getUserName(), equalTo(actualClient.getUserName()));
        }
    }
}
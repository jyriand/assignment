package com.example.assignment.repositories;

import com.example.assignment.model.Country;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class CountryRepositoryTest extends DatabaseTest {

    private CountryRepository countryRepository;

    @BeforeEach
    void setUp() {
        countryRepository = new CountryRepository(namedJdbcTemplate);
    }

    @Test
    void hasFiveCountriesByDefault() {
        List<Country> allCountries = countryRepository.getAll();
        assertThat(allCountries, hasSize(5));
    }

    @Test
    void mapsCountries() {
        List<Country> allCountries = countryRepository.getAll();
        Country country = allCountries.get(0);

        assertThat(country.getCountryCode(), equalTo("EE"));
        assertThat(country.getName(), equalTo("Estonia"));
    }
}
package com.example.assignment.repositories;

import com.example.assignment.model.ApplicationUser;
import com.example.assignment.model.UserName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.example.assignment.model.ApplicationUser.newApplicationUser;
import static com.example.assignment.model.UserName.userName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

class UserReporitoryTest extends DatabaseTest {

    private UserReporitory userReporitory;

    @BeforeEach
    void setUp() {
        cleanup();
        userReporitory = new UserReporitory(jdbcTemplate);
    }

    @Test
    void returnsEmptyOptionalWhenUserDoesntExist() {
        UserName userName = userName("test_user");
        Optional<ApplicationUser> user = userReporitory.findUser(userName);

        assertThat(user.isPresent(), equalTo(false));
    }

    @Test
    void findsExistingUser() {
        ApplicationUser createdUser = createUser();

        Optional<ApplicationUser> maybeUser = userReporitory.findUser(createdUser.getUsername());

        assertThat(maybeUser.isPresent(), equalTo(true));

        maybeUser.ifPresent((user) -> {
            assertThat(user.getUsername(), equalTo(createdUser.getUsername()));
            assertThat(user.getPassword(), equalTo(createdUser.getPassword()));
        });
    }

    private ApplicationUser createUser() {
        jdbcTemplate.execute("insert into application_user(username,password) values ('testuser', 'password')");

        return newApplicationUser()
                .withUsername("testuser")
                .withPassword("password")
                .build();
    }
}
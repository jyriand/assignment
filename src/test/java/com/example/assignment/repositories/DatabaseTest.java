package com.example.assignment.repositories;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@SpringBootTest
public class DatabaseTest {

    @Autowired
    protected NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected Flyway flyway;

    protected void cleanup() {
        flyway.clean();
        flyway.migrate();
    }
}

package com.example.assignment.controller;

import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserClientForm;
import com.example.assignment.model.UserName;
import com.example.assignment.service.CountryService;
import com.example.assignment.service.UserClientsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.example.assignment.fixtures.Fixtures.userClientForm;
import static com.example.assignment.model.UserName.userName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ClientsController.class)
class ClientsControllerTest {

    @MockBean
    private UserClientsService userClientsService;

    @MockBean
    private CountryService countryService;

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(value = "test_user")
    @Test
    void returnsOK() throws Exception {
        mockMvc.perform(get("/clients")).andExpect(status().isOk());
    }

    @WithMockUser(value = "test_user")
    @Test
    void viewName() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/clients")).andReturn();

        ModelAndView modelAndView = mvcResult.getModelAndView();
        assertThat(modelAndView.getViewName(), equalTo("clients"));
    }

    @WithMockUser(value = "test_user")
    @Test
    void modelContainsClients() throws Exception {
        List<UserClient> testClients = testClients();
        when(userClientsService.getUserClients(userName("test_user"))).thenReturn(testClients);
        MvcResult mvcResult = mockMvc.perform(get("/clients")).andReturn();

        Map<String, Object> model = mvcResult.getModelAndView().getModel();
        assertThat(model, hasKey("clients"));
        assertThat(model.get("clients"), equalTo(testClients));
    }

    @WithMockUser(value = "test_user")
    @Test
    void addNewClient() throws Exception {
        UserClientForm form = userClientForm();
        UserName userName = UserName.userName("test_user");

        mockMvc.perform(post("/clients/add")
                .with(csrf())
                .param("username", form.getUsername())
                .param("firstName", form.getFirstName())
                .param("lastName", form.getLastName())
                .param("email", form.getEmail())
                .param("address", form.getAddress())
                .param("country", form.getCountry()))
                .andExpect(status().is3xxRedirection());

        then(userClientsService).should().addUserClient(userName, form);
    }

    private List<UserClient> testClients() {
        UserClient c1 = UserClient.builder().build();
        UserClient c2 = UserClient.builder().build();
        UserClient c3 = UserClient.builder().build();
        return Arrays.asList(c1, c2, c3);
    }
}
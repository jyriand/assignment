package com.example.assignment.model;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserNameTest {

    @Test
    void usernameCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            UserName.userName(null);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            UserName.userName("");
        });
    }

    @Test
    void canCreateUsername() {
        UserName test = UserName.userName("test");
        assertThat(test.getUserName(), equalTo("test"));

    }
}
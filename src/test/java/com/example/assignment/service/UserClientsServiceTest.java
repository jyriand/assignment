package com.example.assignment.service;

import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserName;
import com.example.assignment.repositories.UserClientsRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static com.example.assignment.fixtures.Fixtures.userClientForm;
import static com.example.assignment.model.UserClientForm.toUserClient;
import static com.example.assignment.model.UserName.userName;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@ExtendWith(MockitoExtension.class)
class UserClientsServiceTest {

    @Mock
    private UserClientsRepository repository;

    private UserClientsService service;

    @BeforeEach
    void setUp() {
        service = new UserClientsService(repository);
    }

    @Nested
    class FindUserClients {
        @Test
        void findUserClients() {
            UserName username = userName("test");
            given(repository.findUserClients(username)).willReturn(userClients());

            List<UserClient> userClients = service.getUserClients(username);

            assertThat(userClients, hasSize(1));
        }

        private List<UserClient> userClients() {
            UserClient userClient = UserClient.builder().build();

            return Arrays.asList(userClient);
        }
    }

    @Nested
    class AddUserClient {

        @Test
        void convertsFormToUserClientAndSaves() {
            UserClient userClient = toUserClient(userClientForm());
            UserName userName = userName("test");
            service.addUserClient(userName, userClientForm());
            then(repository).should().saveUserClient(userName, userClient);
        }
    }

}
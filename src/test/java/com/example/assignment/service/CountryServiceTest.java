package com.example.assignment.service;

import com.example.assignment.model.Country;
import com.example.assignment.repositories.CountryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CountryServiceTest {

    @Mock
    private CountryRepository countryRepository;

    @InjectMocks
    private CountryService countryService;

    @Test
    void getsAvailableCountries() {
        List<Country> countries = countries();
        when(countryRepository.getAll()).thenReturn(countries);

        List<Country> availableCountries = countryService.getAvailableCountries();

        assertThat(availableCountries, equalTo(countries));
    }


    private List<Country> countries() {
        return Arrays.asList(
                new Country("ET", "Estonia"),
                new Country("LV", "Latvia"));
    }
}
package com.example.assignment.auth;

import com.example.assignment.model.ApplicationUser;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.List;

import static com.example.assignment.model.ApplicationUser.newApplicationUser;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

class UserPrincipalTest {


    private ApplicationUser applicationUser = newApplicationUser()
            .withUsername("test_user")
            .withPassword("test_password")
            .build();

    private UserDetails userDetails = new UserPrincipal(applicationUser);


    @Test
    void getUsername() {
        assertThat(userDetails.getUsername(),
                equalTo(applicationUser.getUsername().getUserName()));
    }

    @Test
    void getPassword() {
        assertThat(userDetails.getPassword(), equalTo(applicationUser.getPassword()));
    }

    @Test
    void getAuthorities() {
        List<? extends GrantedAuthority> authorities = new ArrayList<>(userDetails.getAuthorities());

        assertThat(authorities, hasSize(1));
        assertThat(authorities.get(0).getAuthority(), equalTo("USER"));
    }

    @Test
    void isAccountNonExpired() {
        assertThat(userDetails.isAccountNonExpired(), equalTo(true));
    }

    @Test
    void isAccountNonLocked() {
        assertThat(userDetails.isAccountNonLocked(), equalTo(true));
    }

    @Test
    void isCredentialsNonExpired() {
        assertThat(userDetails.isCredentialsNonExpired(), equalTo(true));
    }

    @Test
    void isEnabled() {
        assertThat(userDetails.isEnabled(), equalTo(true));
    }
}
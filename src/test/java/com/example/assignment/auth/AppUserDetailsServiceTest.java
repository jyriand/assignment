package com.example.assignment.auth;

import com.example.assignment.model.ApplicationUser;
import com.example.assignment.model.UserName;
import com.example.assignment.repositories.UserReporitory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static com.example.assignment.model.ApplicationUser.newApplicationUser;
import static com.example.assignment.model.UserName.userName;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class AppUserDetailsServiceTest {

    @Mock
    private UserReporitory reporitory;

    private UserDetailsService service;

    @BeforeEach
    void setUp() {
        service = new AppUserDetailsService(reporitory);
    }

    @Test
    void loadsUserFromUserRepository() {
        UserName username = userName("user1");
        given(reporitory.findUser(username)).willReturn(testUser(username));

        UserDetails user = service.loadUserByUsername(username.getUserName());
        assertThat(user.getUsername(), equalTo(username.getUserName()));
    }

    @Test
    void throwsExceptionWhenUserIsNotFound() {
        given(reporitory.findUser(any())).willReturn(Optional.empty());

        assertThrows(UsernameNotFoundException.class, () -> {
            service.loadUserByUsername("username");
        });
    }

    @Test
    void usernameCantBeEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            service.loadUserByUsername(null);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            service.loadUserByUsername("");
        });

        assertThrows(IllegalArgumentException.class, () -> {
            service.loadUserByUsername("      ");
        });
    }

    private Optional<ApplicationUser> testUser(UserName username) {
        return Optional.of(newApplicationUser()
                .withUsername(username)
                .build());
    }
}
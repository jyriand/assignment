-- Create a table for applicatio users
CREATE TABLE user_client
(
    id                    IDENTITY     NOT NULL PRIMARY KEY,
    username              VARCHAR(80)  NOT NULL UNIQUE,
    firstName             VARCHAR(255) NOT NULL,
    lastName              VARCHAR(255) NOT NULL,
    email                 VARCHAR(255),
    address               VARCHAR(255) NOT NULL,
    country               VARCHAR(255),
    application_user_name VARCHAR(255) NOT NULL
);

CREATE TABLE countries
(
    country_code varchar(2) PRIMARY KEY,
    name         varchar(80) not null
);

-- Add countries
insert into countries(country_code, name)
values ('EE', 'Estonia');
insert into countries(country_code, name)
values ('LV', 'Latvia');
insert into countries(country_code, name)
values ('LT', 'Lithuania');
insert into countries(country_code, name)
values ('FI', 'Finland');
insert into countries(country_code, name)
values ('SE', 'Sweden');
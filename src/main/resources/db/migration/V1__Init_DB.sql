-- Create a table for applicatio users
CREATE TABLE application_user (
  id IDENTITY NOT NULL PRIMARY KEY,
  username varchar(80) not null,
  password char(60) not null
);


-- Add three dummy users. Password is 'pass'
insert into application_user(username, password)
  values('user1', '$2b$10$P4C86VxHHR8cbPGUmvK/8uuEvfVeoOeTZn98l5VknhXTBr0Hsyyk.');

insert into application_user(username, password)
  values('user2', '$2b$10$P4C86VxHHR8cbPGUmvK/8uuEvfVeoOeTZn98l5VknhXTBr0Hsyyk.')

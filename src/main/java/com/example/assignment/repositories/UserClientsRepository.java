package com.example.assignment.repositories;

import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserName;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserClientsRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public UserClientsRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<UserClient> findUserClients(UserName userName) {
        Map<String, String> mapper = new HashMap<>();
        mapper.put("userName", userName.getUserName());
        return jdbcTemplate.query("select * from user_client where application_user_name = :userName", mapper,
                (rs, rowNum) -> toUserClient(rs));
    }

    public void saveUserClient(UserName userName, UserClient userClient) {
//        jdbcTemplate.update("INSERT INTO user_client(username, firstName, lastName, email, address, country, application_user_name)" +
//                        " values (:userName,:firstName,:lastName,:email,:address,:country,:applicationUser)",
//                new MapSqlParameterSource("userName", userClient.getUserName())
//                        .addValue("firstName", userClient.getFirstName())
//                        .addValue("lastName", userClient.getLastName())
//                        .addValue("email", userClient.getEmail())
//                        .addValue("address", userClient.getAddress())
//                        .addValue("country", userClient.getCountry())
//                        .addValue("applicationUser", userName.getUserName()));

        jdbcTemplate.update("MERGE INTO user_client(username, firstName, lastName, email, address, country, application_user_name) KEY(username) " +
                        " values (:userName,:firstName,:lastName,:email,:address,:country,:applicationUser)",
                new MapSqlParameterSource("userName", userClient.getUserName())
                        .addValue("firstName", userClient.getFirstName())
                        .addValue("lastName", userClient.getLastName())
                        .addValue("email", userClient.getEmail())
                        .addValue("address", userClient.getAddress())
                        .addValue("country", userClient.getCountry())
                        .addValue("applicationUser", userName.getUserName()));
    }

    public Optional<UserClient> findUserClientByUsername(UserName userName, String clientUserName) {
        Map<String, String> mapper = new HashMap<>();
        mapper.put("clientUserName", clientUserName);
        mapper.put("userName", userName.getUserName());

        try {
            UserClient userClient =
                    jdbcTemplate.queryForObject("select * from user_client where username=:clientUserName and application_user_name= :userName",
                            mapper,
                            (rs, rowNum) -> toUserClient(rs));

            return Optional.ofNullable(userClient);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    private UserClient toUserClient(ResultSet rs) throws SQLException {
        return UserClient.builder()
                .withUserName(rs.getString("username"))
                .withFirstName(rs.getString("firstName"))
                .withLastName(rs.getString("lastName"))
                .withEmail(rs.getString("email"))
                .withAddress(rs.getString("address"))
                .withCountry(rs.getString("country"))
                .build();
    }
}

package com.example.assignment.repositories;

import com.example.assignment.model.Country;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

public class CountryRepository {
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    public CountryRepository(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public List<Country> getAll() {
        return namedJdbcTemplate.query("select * from countries",
                (rs, rowNum) -> new Country(
                        rs.getString("country_code"),
                        rs.getString("name")
                ));
    }
}

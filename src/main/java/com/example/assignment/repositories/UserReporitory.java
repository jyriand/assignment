package com.example.assignment.repositories;

import com.example.assignment.model.ApplicationUser;
import com.example.assignment.model.UserName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;

public class UserReporitory {

    private static final Logger log = LoggerFactory.getLogger(UserReporitory.class);
    private final JdbcTemplate jdbcTemplate;

    public UserReporitory(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Optional<ApplicationUser> findUser(UserName userName) {
        log.info("Querying for user: {}", userName.getUserName());

        String sql = "select * from application_user where username = ?";
        try {
            ApplicationUser user = jdbcTemplate.queryForObject(sql, new Object[]{userName.getUserName()}, (rs, i) -> {
                return ApplicationUser.newApplicationUser()
                        .withUsername(rs.getString("username"))
                        .withPassword(rs.getString("password"))
                        .build();
            });
            return Optional.of(user);
        } catch (EmptyResultDataAccessException ex) {
            log.warn("User {} not found", userName.getUserName());
            return Optional.empty();
        }
    }
}

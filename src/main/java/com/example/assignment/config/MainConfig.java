package com.example.assignment.config;

import com.example.assignment.auth.AppUserDetailsService;
import com.example.assignment.repositories.CountryRepository;
import com.example.assignment.repositories.UserClientsRepository;
import com.example.assignment.repositories.UserReporitory;
import com.example.assignment.service.CountryService;
import com.example.assignment.service.UserClientsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class MainConfig {

    @Bean
    public CountryRepository countryRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        return new CountryRepository(jdbcTemplate);
    }

    @Bean
    public CountryService countryService(CountryRepository countryRepository) {
        return new CountryService(countryRepository);
    }

    @Bean
    public UserClientsRepository userClientsRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        return new UserClientsRepository(jdbcTemplate);
    }

    @Bean
    public UserClientsService userClientsService(
            UserClientsRepository userClientsRepository) {
        return new UserClientsService(userClientsRepository);
    }

    @Bean
    public UserReporitory userReporitory(JdbcTemplate jdbcTemplate) {
        return new UserReporitory(jdbcTemplate);
    }

    @Bean
    public UserDetailsService userDetailsService(UserReporitory userReporitory) {
        return new AppUserDetailsService(userReporitory);
    }
}

package com.example.assignment.controller;

import com.example.assignment.model.Country;
import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserClientForm;
import com.example.assignment.model.UserName;
import com.example.assignment.service.CountryService;
import com.example.assignment.service.UserClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import static com.example.assignment.model.UserName.userName;

@Controller
public class ClientsController {

    private final UserClientsService userClientsService;
    private final CountryService countryService;

    @Autowired
    public ClientsController(UserClientsService userClientsService, CountryService countryService) {
        this.userClientsService = userClientsService;
        this.countryService = countryService;
    }

    @GetMapping("/clients")
    public String clientHome(Principal principal, Model model) {
        UserName userName = userName(principal.getName());
        List<UserClient> userClients = userClientsService.getUserClients(userName);
        model.addAttribute("clients", userClients);
        return "clients";
    }

    @GetMapping("/clients/add")
    public String addClientView(Model model) {
        List<Country> availableCountries = countryService.getAvailableCountries();
        model.addAttribute("countries", availableCountries);
        model.addAttribute("clientForm", new UserClientForm());
        return "add_client";
    }

    @GetMapping("clients/{clientUserName}/edit")
    public String editUserClient(Principal principal, @PathVariable String clientUserName, Model model) {
        UserName userName = userName(principal.getName());
        List<Country> availableCountries = countryService.getAvailableCountries();
        Optional<UserClient> client = userClientsService.getUserClientByUsername(userName, clientUserName);

        if (!client.isPresent()) {
            return "redirect:/clients";
        }

        UserClientForm userClientForm = UserClientForm.fromUserClient(client.get());

        model.addAttribute("clientForm", userClientForm);
        model.addAttribute("countries", availableCountries);
        return "edit_client";
    }

    @PostMapping("/clients/{clientUserName}/edit")
    public String editUserClient(Principal principal,
                                 @Valid
                                 @ModelAttribute("clientForm")
                                         UserClientForm userClientForm,
                                 BindingResult bindingResult,
                                 @PathVariable String clientUserName,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            List<Country> availableCountries = countryService.getAvailableCountries();
            model.addAttribute("countries", availableCountries);
            return "edit_client";
        }
        if (!clientUserName.equals(userClientForm.getUsername())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        UserName userName = userName(principal.getName());
        userClientsService.editUser(userName, userClientForm);
        return "redirect:/clients";
    }

    @PostMapping("/clients/add")
    public String addNewClient(Principal principal,
                               @Valid
                               @ModelAttribute("clientForm")
                                       UserClientForm userClientForm,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            List<Country> availableCountries = countryService.getAvailableCountries();
            model.addAttribute("countries", availableCountries);
            return "add_client";
        }
        UserName userName = userName(principal.getName());
        userClientsService.addUserClient(userName, userClientForm);
        return "redirect:/clients";
    }
}

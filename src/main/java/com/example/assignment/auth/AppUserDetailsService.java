package com.example.assignment.auth;

import com.example.assignment.repositories.UserReporitory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static com.example.assignment.model.UserName.userName;

public class AppUserDetailsService implements UserDetailsService {

    private final UserReporitory reporitory;

    public AppUserDetailsService(UserReporitory reporitory) {
        this.reporitory = reporitory;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("Username cannot be empty");
        }
        return reporitory.findUser(userName(username))
                .map(UserPrincipal::new)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found."));
    }
}

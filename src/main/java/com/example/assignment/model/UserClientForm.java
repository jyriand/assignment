package com.example.assignment.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Objects;

public class UserClientForm {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @NotEmpty
    private String username;

    @Email
    private String email;

    @NotEmpty
    private String address;

    @NotEmpty
    private String country;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserClientForm that = (UserClientForm) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(username, that.username) &&
                email.equals(that.email) &&
                Objects.equals(address, that.address) &&
                Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, username, email, address, country);
    }

    public static UserClient toUserClient(UserClientForm form) {
        return UserClient.builder()
                .withFirstName(form.getFirstName())
                .withLastName(form.getLastName())
                .withUserName(form.getUsername())
                .withEmail(form.getEmail())
                .withAddress(form.getAddress())
                .withCountry(form.getCountry())
                .build();
    }

    public static UserClientForm fromUserClient(UserClient userClient) {
        UserClientForm userClientForm = new UserClientForm();
        userClientForm.setUsername(userClient.getUserName());
        userClientForm.setFirstName(userClient.getFirstName());
        userClientForm.setLastName(userClient.getLastName());
        userClientForm.setEmail(userClient.getEmail());
        userClientForm.setAddress(userClient.getAddress());
        userClientForm.setCountry(userClient.getCountry());
        return userClientForm;
    }
}

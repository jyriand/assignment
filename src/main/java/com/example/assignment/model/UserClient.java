package com.example.assignment.model;

import java.util.Objects;

public class UserClient {
    private final String userName;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String address;
    private final String country;

    private UserClient(Builder builder) {
        userName = builder.userName;
        firstName = builder.firstName;
        lastName = builder.lastName;
        email = builder.email;
        address = builder.address;
        country = builder.country;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builderFrom(UserClient userClient) {
        return new Builder()
                .withUserName(userClient.getUserName())
                .withFirstName(userClient.getFirstName())
                .withLastName(userClient.getLastName())
                .withEmail(userClient.getEmail())
                .withAddress(userClient.getAddress())
                .withCountry(userClient.getCountry());
    }

    public String getUserName() {
        return userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getCountry() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserClient that = (UserClient) o;
        return userName.equals(that.userName) &&
                firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                Objects.equals(email, that.email) &&
                address.equals(that.address) &&
                country.equals(that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, firstName, lastName, email, address, country);
    }

    public static final class Builder {
        private String userName;
        private String firstName;
        private String lastName;
        private String email;
        private String address;
        private String country;

        private Builder() {
        }

        public Builder withUserName(String val) {
            userName = val;
            return this;
        }

        public Builder withFirstName(String val) {
            firstName = val;
            return this;
        }

        public Builder withLastName(String val) {
            lastName = val;
            return this;
        }

        public Builder withEmail(String val) {
            email = val;
            return this;
        }

        public Builder withAddress(String val) {
            address = val;
            return this;
        }

        public Builder withCountry(String val) {
            country = val;
            return this;
        }

        public UserClient build() {
            return new UserClient(this);
        }
    }
}

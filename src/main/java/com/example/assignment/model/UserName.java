package com.example.assignment.model;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class UserName {

    private final String userName;

    private UserName(String userName) {
        this.userName = userName;
    }

    public static UserName userName(String userName) {
        if (StringUtils.isBlank(userName)) {
            throw new IllegalArgumentException("Username can't be empty");
        }
        return new UserName(userName);
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserName userName1 = (UserName) o;
        return Objects.equals(userName, userName1.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName);
    }
}

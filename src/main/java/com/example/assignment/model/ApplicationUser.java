package com.example.assignment.model;

public class ApplicationUser {

    private final UserName username;

    private final String password;

    public UserName getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    private ApplicationUser(Builder builder) {
        username = builder.username;
        password = builder.password;
    }

    public static Builder newApplicationUser() {
        return new Builder();
    }

    public static final class Builder {
        private UserName username;
        private String password;

        private Builder() {
        }

        public Builder withUsername(String val) {
            username = UserName.userName(val);
            return this;
        }

        public Builder withUsername(UserName val) {
            username = val;
            return this;
        }

        public Builder withPassword(String val) {
            password = val;
            return this;
        }

        public ApplicationUser build() {
            return new ApplicationUser(this);
        }
    }
}

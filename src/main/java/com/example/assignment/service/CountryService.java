package com.example.assignment.service;

import com.example.assignment.model.Country;
import com.example.assignment.repositories.CountryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CountryService {
    private static final Logger log = LoggerFactory.getLogger(CountryService.class);

    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAvailableCountries() {
        log.info("Fetching available countries");
        return countryRepository.getAll();
    }
}

package com.example.assignment.service;

import com.example.assignment.model.UserClient;
import com.example.assignment.model.UserClientForm;
import com.example.assignment.model.UserName;
import com.example.assignment.repositories.UserClientsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

public class UserClientsService {
    private UserClientsRepository repository;

    public UserClientsService(UserClientsRepository repository) {
        this.repository = repository;
    }

    public List<UserClient> getUserClients(UserName userName) {
        return repository.findUserClients(userName);
    }

    public void addUserClient(UserName userName, UserClientForm form) {
        UserClient userClient = UserClientForm.toUserClient(form);
        repository.saveUserClient(userName, userClient);
    }

    public Optional<UserClient> getUserClientByUsername(UserName userName, String clientUserName) {
        return repository.findUserClientByUsername(userName, clientUserName);
    }

    public void editUser(UserName userName, UserClientForm userClientForm) {
        Optional<UserClient> userClient = repository.findUserClientByUsername(userName, userClientForm.getUsername());
        if (!userClient.isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        UserClient userClient1 = UserClientForm.toUserClient(userClientForm);

        repository.saveUserClient(userName, userClient1);

    }
}
